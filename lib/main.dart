import 'package:flutter/material.dart';
import 'package:news_app/app.dart';

void main() => runApp(NewsApp());

class NewsApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: App(),
    );
  }
}
